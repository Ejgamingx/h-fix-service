<style>

</style>
<div class="col-12">
          <div class="card">
          	<div class="card-body">
              <h3>How to ADD a Ticket</h3></p>
              <div>
                <h2>Step 1:</h2>
                <img src="images/cusaddticket.png" style="float:left; margin-right:20px"/>
                <h3 style="margin-top:50px">Go to the sidebar menu and select the Ticket and then click on Add New.</h3>
                <hr style="clear: both" />
                <h2>Step 2:</h2>
                <img src="images/custicketdescription.png" style="float:left; margin-right:20px; height: 380px;" />
                <h3 style="margin-top:50px">1. Now Add the Subject or Title on what the problem that needed to be fixed</h3>
                <h3 style="margin-top:50px">2. Select a existing Department for the job so that they can fix the problem</h3>
                <h3 style="margin-top:50px">3. Add a Descriptions about the problem in your home/house</h3>
                <h3 style="margin-top:50px">4. Click save to create the ticket</h3>
                <hr style="clear: both" />
                <h2>Step 3:</h2>
                <img src="images/custicketsave.png" style="float:left; margin-right:20px" />
                <h3 style="margin-top:50px">Go to the sidebar menu to View the ticket (Ticket List/Payment)</h3>
                <hr style="clear: both" />
                <h2>Step 4:</h2>
                <img src="images/custicketcomment.png" style="float:left; margin-right:20px; height: 300px;" />
                <h3 style="margin-top:50px">Wait for the Staff to update the status to processing and use the comment section for the agreement on the job that needed to be fixed. The staff will interact on the ticket if they are available to work.</h3>
                <hr style="clear: both" />
</div>


 <div style="clear: both;"></div>