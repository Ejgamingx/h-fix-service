<!DOCTYPE html>
<html lang="en">
<?php session_start() ?>
<?php 
	if(!isset($_SESSION['login_id']))
	    header('location:login.php');
	include 'header.php' 
?>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <?php include 'topbar.php' ?>
  <?php include 'sidebar.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	 <div class="toast" id="alert_toast" role="alert" aria-live="assertive" aria-atomic="true">
	    <div class="toast-body text-white">
	    </div>
	  </div>
    <div id="toastsContainerTopRight" class="toasts-top-right fixed"></div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payment Deals</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
            <hr class="border-primary">
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
        <div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<table class="table">
  <thead>
    <tr>
      <th scope="col">Email of the Worker</th>
      <th scope="col">Amount to be paid</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>123@gmail.com</td>
      <td>₱100.00</td>
      <td> 
            <a href="./paynow.php" class="btn btn-success" role="button"><i class="fas fa-edit"></i>Pay now</a>
            <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i>Reject</button></td>
    </tr>
  </tbody>
</table>
		</div>
		<div class="col-md-12">
                <a class="btn btn-primary float-right" onclick="goBack()" role="button">Back</a>
							</div>
      </div><!--/. container-fluid -->
    </section>
   
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
  
    <div class="float-right d-none d-sm-inline-block">
      <b>H-Fix</b>
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<!-- Bootstrap -->
<script>
function goBack() {
  window.history.back();
}
</script>
<?php include 'footer.php' ?>
</body>
</html>
