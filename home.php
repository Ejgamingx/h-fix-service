<?php include('db_connect.php') ?>

<!-- Info boxes -->
<?php if($_SESSION['login_type'] == 1): ?>
<!--dashboard-->
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<?php
	//require('db_con.php');

	$sql = "SELECT COUNT(*) as count FROM `customers` WHERE 1 ";

	$customers = mysqli_query($conn,$sql);

	$customers = mysqli_fetch_all($customers,MYSQLI_ASSOC);

	$customers = json_encode(array_column($customers, 'count'),JSON_NUMERIC_CHECK);
	
	/* Getting staff table data */
	$sql = "SELECT COUNT(*) as count FROM `staff` WHERE 1 ";

	$staff = mysqli_query($conn,$sql);

	$staff = mysqli_fetch_all($staff,MYSQLI_ASSOC);

	$staff = json_encode(array_column($staff, 'count'),JSON_NUMERIC_CHECK);

	/* Getting departments table data */
	$sql = "SELECT COUNT(*) as count FROM `departments` WHERE 1 ";

	$departments = mysqli_query($conn,$sql);

	$departments = mysqli_fetch_all($departments,MYSQLI_ASSOC);

	$departments = json_encode(array_column($departments, 'count'),JSON_NUMERIC_CHECK);
	
	/* Getting tickets table data */
	$sql = "SELECT COUNT(*) as count FROM `tickets` WHERE 1 ";

	$tickets = mysqli_query($conn,$sql);

	$tickets = mysqli_fetch_all($tickets,MYSQLI_ASSOC);

	$tickets = json_encode(array_column($tickets, 'count'),JSON_NUMERIC_CHECK);

?>


<script type="text/javascript">


$(function () { 


    var data_customers = <?php echo $customers; ?>;
	var data_staff = <?php echo $staff; ?>;
	var data_departments = <?php echo $departments; ?>;
	var data_tickets = <?php echo $tickets; ?>;
	
    $('#container').highcharts({

        chart: {

            type: 'column'

        },
		credits: {
		enabled: false
		},

        title: {

            text: 'Data of Categories'

        },

        xAxis: {

            categories: ['Total']

        },

        yAxis: {

            title: {

                text: 'Count'

            }

        },

        series: [{

            name: 'Customers',

            data: data_customers

        }, {

            name: 'Staff',

            data: data_staff,
			color: 'red'

        },
		{

            name: 'Departments',

            data: data_departments,
			color: 'green'

        },
		{

            name: 'Tickets',

            data: data_departments,
			color: 'yellow'

        }]

    });

});


</script>

<style>
.highcharts-figure, .highcharts-data-table table {
    min-width: 320px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}


input[type="number"] {
	min-width: 50px;
}
</style>

<div class="container">


	<h2 class="text-center"></h2>

    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div id="container"></div>

                </div>

            </div>

        </div>

    </div>

</div>
<br>
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Customers</span>
                <span class="info-box-number">
                  <?php echo $conn->query("SELECT * FROM customers")->num_rows; ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Staff</span>
                 <span class="info-box-number">
                  <?php echo $conn->query("SELECT * FROM staff")->num_rows; ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-columns"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Departments</span>
                <span class="info-box-number"><?php echo $conn->query("SELECT * FROM departments")->num_rows; ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-ticket-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Tickets</span>
                <span class="info-box-number"><?php echo $conn->query("SELECT * FROM tickets")->num_rows; ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
<?php else: ?>
	 <div class="col-12">
          <div class="card">
          	<div class="card-body">
          		Welcome <?php echo $_SESSION['login_name'] ?>!
          	</div>
          </div>
      </div>
<?php if($_SESSION['login_type'] == 3): ?>
<?php include 'customerdash.php' ?>
<?php endif; ?>
<?php if($_SESSION['login_type'] == 2): ?>
<?php include 'staffdash.php' ?>
<?php endif; ?>


<?php endif; ?>

