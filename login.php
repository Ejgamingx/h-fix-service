<!DOCTYPE html>
<html lang="en">
<?php 
session_start();
include('./db_connect.php');
?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Login | H-Fix</title>
 	

<?php include('./header.php'); ?>
<?php 
if(isset($_SESSION['login_id']))
header("location:index.php?page=home");

?>

</head>
<style>
body{
		width: 100%;
	    height: calc(100%);
	    position: fixed;
	    top:0;
	    left: 0
	    /*background: #007bff;*/
	}
	main#main{
		width:100%;
		height: calc(100%);
		display: flex;
	}
	img{
		display: block;	
  		margin-left: auto;
  		margin-right: auto;
  		width: 29%;
	}
	body{
		/* background: rgb(132,130,167);
background: linear-gradient(90deg, rgba(132,130,167,1) 0%, rgba(187,190,205,1) 0%, rgba(9,86,121,0.5158438375350141) 17%, rgba(1,91,149,0.5158438375350141) 100%); */
	}
	.card-body {
		
		font-weight: bold;
  background-color: lightblue;    
  /* text-align: center; */
 
}
.card{

  background-color: lightblue;    
  /* text-align: center; */
  padding: 5px;
  box-shadow: 3px 6px #d9d9d9;

}
.btn-sm{
 
	border: 1px solid;
	background-color: lightblue;
  	color: black;
  	-webkit-border-radius: 8px;
  	-moz-border-radius: 8px;
  	border-radius: 15px 15px;
}


</style>

<body class="bg-white">


  <main id="main" >
  	
  		<div class="align-self-center w-100">
		  <img src="sdasdadssdasd.png" alt="Avatar" class="image" alt="Simply Easy Learning" width="500"
         height="100">
  		<div id="login-center" class="row justify-content-center">
  			<div class="card col-md-4">
			
  					<form id="login-form" >
  						<div class="form-group">
  							<label for="username" class="control-label text-dark">Username</label>
  							<input type="text" id="username" name="username" class="form-control form-control-sm">
  						</div>
  						<div class="form-group">
  							<label for="password" class="control-label text-dark">Password</label>
  							<input type="password" id="password" name="password" class="form-control form-control-sm">
  						</div>
  						<div class="form-group">
  							<label for="password" class="control-label text-dark">Type</label>
  							<select class="custom-select custom-select-sm" name="type">
  								<option value="3">Customer</option>
  								<option value="2">Staff</option>
  								<option value="1">Admin</option>
  								
  							</select>
  						</div>
          
              <div class="container">
<div class="row" align="center">
    <div class="box col"><a href="register.php"><input class="btn-sm btn-block btn-wave col-md-8 btn-primary" type="button" value="Register"></a></div>
    <div class="box col"><button class="btn-sm btn-block btn-wave col-md-8 btn-primary">Login</button></div>
  </div>
</div>
  					</form>
  				</div>
  			</div>
  		</div>
  		</div>
  </main>

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>


</body>
<script>
	$('#login-form').submit(function(e){
		e.preventDefault()
		$('#login-form button[type="button"]').attr('disabled',true).html('Logging in...');
		if($(this).find('.alert-danger').length > 0 )
			$(this).find('.alert-danger').remove();
		$.ajax({
			url:'ajax.php?action=login',
			method:'POST',
			data:$(this).serialize(),
			error:err=>{
				console.log(err)
		$('#login-form button[type="button"]').removeAttr('disabled').html('Login');

			},
			success:function(resp){
				if(resp == 1){
					location.href ='index.php?page=home';
				}else{
					$('#login-form').prepend('<div class="alert alert-danger">Username or password is incorrect.</div>')
					$('#login-form button[type="button"]').removeAttr('disabled').html('Login');
				}
			}
		})
	})
	$('.number').on('input',function(){
        var val = $(this).val()
        val = val.replace(/[^0-9 \,]/, '');
        $(this).val(val)
    })
</script>	
</html>