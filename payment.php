<!DOCTYPE html>
<html lang="en">
<?php session_start() ?>
<?php 
	if(!isset($_SESSION['login_id']))
	    header('location:login.php');
	include 'header.php' 
?>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <?php include 'topbar.php' ?>
  <?php include 'sidebar.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	 <div class="toast" id="alert_toast" role="alert" aria-live="assertive" aria-atomic="true">
	    <div class="toast-body text-white">
	    </div>
	  </div>
    <div id="toastsContainerTopRight" class="toasts-top-right fixed"></div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payment</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
            <hr class="border-primary">
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
        <div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<form action="">
					<div class="col-md-6">
						<label for="" class="control-label">Amount</label>
						<div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text">₱</span>
  </div>
  <input type="text" class="form-control" aria-label="Amount" placeholder="Insert Amount to be Paid">
  <div class="input-group-append">
    <span class="input-group-text">.00</span>
  </div>
</div>

						<div class="form-group">
							<label for="" class="control-label">Account</label>
							<input type="email" name="email" class="form-control form-control-sm" placeholder="Insert the email of the account that will pay for the service">
						</div>
					
					<div class="form-group">
							<label for="" class="control-label">Account for payment</label>
							<input type="accn" name="accn" class="form-control form-control-sm" placeholder="Insert the account number to send the payment">
					</div>
			</form>
		</div>
		<div class="col-md-12">
								<button class="btn btn-primary float-right">Add Payment</button>
							</div>
      </div><!--/. container-fluid -->
    </section>
   
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">

    <div class="float-right d-none d-sm-inline-block">
      <b>H-Fix</b>
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<!-- Bootstrap -->
<?php include 'footer.php' ?>
</body>
</html>
