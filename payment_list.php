<?php include'db_connect.php' ?>
<style>
.total{
    margin-bottom:20px;
    text-align: center;
    font-size: 40px;
}
</style>
<?php   
                        $count = $conn->query("SELECT amount FROM payment");
                        $total = 0;
                        while($row = mysqli_fetch_assoc($count)) {
                            $total += $row['amount'];
                        }
?>

<div class="total">Total Amount: <?php echo $total; ?></div>


<div class="col-lg">
	<div class="card">
		<div class="card-body">
			<table class="table tabe-hover table-bordered" id="list">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">ticket_id</th>
						<th class="text-center">payment_method</th>
						<th class="text-center">amount</th>
						<th class="text-center">account_details</th>
						<th class="text-center">customer</th>
						<th class="text-center">staff</th>
                        <th class="text-center">date</th>
                        <th class="text-center">action</th>
					</tr>
				</thead>
                
				<tbody>
					<?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM payment");
					while($row= $qry->fetch_assoc()):
					?>
					<tr>
						<th class="text-center"><?php echo $i++ ?></th>
						<td class="text-center"><b><?php echo $row['ticket_id'] ?></b></td>
						<td class="text-center"><b><?php echo $row['payment_method'] ?></b></td>
						<td class="text-center"><b><?php echo $row['amount'] ?></b></td>
						<td class="text-center"><b><?php echo $row['account_details'] ?></b></td>
                        <td class="text-center"><b><?php echo $row['customer'] ?></b></td>
						<td class="text-center"><b><?php echo $row['staff'] ?></b></td>
                        <td class="text-center"><b><?php echo $row['created_date'] ?></b></td>
                        <td class="text-center">
                        <button type="button" class="btn btn-default" >
                        <a class="dropdown-item payment_view" href="./index.php?page=payment_view&id=<?php echo $row['ID'] ?>" data-id="<?php echo $row['ID'] ?>">View</a>

                        </button>
				    	</td>	
					</tr>	
				</tbody>
                <?php endwhile; ?>
			</table>
			
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#list').dataTable()
	})
</script>