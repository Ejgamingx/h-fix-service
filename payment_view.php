<?php include'db_connect.php' ?>



<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<table class="table tabe-hover table-bordered" id="list">
				<thead>
					<tr>
						<th  class="text-center">#</th>
						<th  class="text-center">Ticket Number</th>
                        <th  class="text-center">Staff</th>
                        <th  class="text-center">Amount</th>
						<th  class="text-center">Proof</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
                    $pic = "";
					$qry = $conn->query("SELECT * FROM payment");
					while($row= $qry->fetch_assoc()):
                    $pic = $row['filename'];
					?>
					<tr>
						<th class="text-center"><?php echo $i++ ?></th>
						<td  class="text-center"><b><?php echo ucwords($row['ticket_id']) ?></b></td>
                        <td  class="text-center"><b><?php echo ucwords($row['staff']) ?></b></td>
                        <td  class="text-center"><b><?php echo ucwords($row['amount']) ?></b></td>
                        <td  class="text-center"><img src="uploads/<?php echo $pic; ?>"  alt="" style="height: 400px; width:250px;" ></td>
					</tr>	
				<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#list').dataTable()

	})
</script>