<!DOCTYPE html>
 <?php
 include('./db_connect.php');
 if (isset($_POST["register"]))

    {
	$id = $_GET["id"];
  $payment_method = $_POST["payment_method"];
	$amount = $_POST["amount"];
	$account_details = $_POST["account_details"];
	$filename = $_FILES["uploadfile"]["name"];
	$tempname = $_FILES["uploadfile"]["tmp_name"];	
	$folder = "uploads/".$filename;
    try {
		
		$qry2 = $conn->query("SELECT * from tickets where ID = $id");
					
					
		while($row2= $qry2->fetch_assoc()):
		$customer = $row2["customer_id"];
		$staff = $row2["staff_id"];
			endwhile;
   // insert in payment table
        $sql = "INSERT INTO payment(ticket_id, payment_method, amount, account_details, customer, staff, filename) VALUES ($id, '" . $payment_method . "', '" . $amount . "', '" . $account_details . "', $customer, $staff, '$filename')";
        mysqli_query($conn, $sql);

        if (move_uploaded_file($tempname, $folder)) {
          $msg = "Image uploaded successfully";
        }else{
          $msg = "Failed to upload image";
      }

        header("Location: index.php?page=view_ticket&id=" .$id);
        exit();
		
    } catch (Exception $e) {
        
    }
	
	}
	
	?>
<html lang="en">
<?php session_start() ?>
<?php 
  if(!isset($_SESSION['login_id']))
      header('location:login.php');
  include 'header.php' 
?>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <?php include 'topbar.php' ?>
  <?php include 'sidebar.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <div class="toast" id="alert_toast" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="toast-body text-white">
      </div>
    </div>
    <div id="toastsContainerTopRight" class="toasts-top-right fixed"></div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payment Method</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
            <hr class="border-primary">
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
<div class="col-lg-12">
  <div class="card">
    <div class="card-body">
   
          <div class="col-md-6">
		  <form method="POST" action="" enctype="multipart/form-data" class="form-horizontal">
            <label for="" class="control-label">Amount</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">₱</span>
                </div>
                <input type="text" class="form-control" id="amount" name="amount" aria-label="Amount" placeholder="Amount to be Paid">
                <div class="input-group-append">
                  <span class="input-group-text">.00</span>
                </div>
              </div>
            <div class="form-group">
            <label for="" class="control-label">Staff </label>
							<select name="account_details" id="account_details" class="custom-select custom-select-sm select2">
								<option value=""></option>
							<?php
								$staffs = $conn->query("SELECT *,concat(lastname,', ',firstname,' ',middlename) as name FROM staff");
								while($row = $staffs->fetch_assoc()):
							?>
								<option value="<?php echo $row['name'] ?>" <?php echo isset($staffs_id) && $staffs_id == $row['name'] ? "selected" : '' ?>><?php echo ucwords($row['name']) ?></option>
							<?php endwhile; ?>
							</select>
            </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
              <div class="card ">
                <div class="card-header">
                    <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                        <!-- Credit card form tabs -->
                        <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                           <!-- <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <i class="fas fa-credit-card mr-2"></i> Credit Card </a> </li>
                            <li class="nav-item"> <a data-toggle="pill" href="#paypal" class="nav-link "> <i class="fab fa-paypal mr-2"></i> Paypal </a> </li>-->
                            <li class="nav-item"> <a data-toggle="pill" href="#net-banking" class="nav-link "> <i class="fas fa-mobile-alt mr-2"></i> Payment Method </a> </li>
                        </ul>
                    </div> <!-- End -->
                    <!-- Credit card form content -->
                    <div class="tab-content">		    
                    <div id="net-banking"  class="tab-pane fade show active pt-3">
                        <div class="form-group "> <label for="Select Your Bank">
                                <h6>Select your Bank</h6>
                            </label> <select class="form-control" id="payment_method" name="payment_method">
                                <option value="" selected disabled>--Please select your Bank--</option>
                                <option value="GCash" >GCash</option>
                                <option value="PayMaya" >PayMaya</option>
                                <option value="CashOnDelivery" >CashOnDelivery</option>
                               
                            </select> </div>
                        <div class="form-group">
						<div class="modal-body">
                       <!--upload-->
						<div class="form-group ">
							<label for="Select Your Bank">
                                <h6>Upload Receipt</h6>
                            </label> 

						

								

								<div id="dropDownSelect1"></div>

								<input type="file" name="uploadfile" value=""/>
									
								<!--<div>
									<button type="submit" name="upload">CLICK HERE TO UPLOAD</button>
									</div>-->
						
						
 						</div>
                         <!--end -->
                    </div>
					
                            <p> <button type="submit" name="register" class="btn btn-primary "><i class="fas fa-mobile-alt mr-2"></i> Proceed Payment</button> </p>
                        </div>
                       <!-- <p class="text-muted">Note: After clicking on the button, you will be directed to a secure gateway for payment. After completing the payment process, you will be redirected back to the website to view details of your order. </p>-->
                    </div> 
				  </form>
					<!-- End -->
                    <!-- End -->
                </div>
          </div>
          </div>

    </div>
    <!--<div class="col-md-12">-->
                <!--<button class="btn btn-primary float-right">Save</button>
              </div>
      </div><!--/. container-fluid -->
    </section>
   
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    
    <div class="float-right d-none d-sm-inline-block">
      <b>H-Fix</b>
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<!-- Bootstrap -->
<?php include 'footer.php' ?>
</body>
</html>
