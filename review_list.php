<?php include'db_connect.php' ?>
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<table class="table tabe-hover table-bordered" id="list">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Username</th>
						<th class="text-center">Rating</th>
						<th class="text-center" >Review</th>
						<th class="text-center">Date</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
                    $id = $_GET['id'];
					$qry = $conn->query("SELECT * FROM review WHERE staff= $id ");


					while($row= $qry->fetch_assoc()):
					?>
					<tr>
						<th class="text-center"><?php echo $i++ ?></th>
						<td class="text-center"><b><?php echo ucwords($row['username']) ?></b></td>
						<td class="text-center"><b><?php echo $row['rating'] ?></b></td>
						<td class="text-center"><b><?php echo $row['review'] ?></b></td>
						<td class="text-center"><b><?php echo $row['created_date'] ?></b></td>
					

					</tr>	
				<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#list').dataTable()
	$('.delete_staff').click(function(){
	_conf("Are you sure to delete this staff?","delete_staff",[$(this).attr('data-id')])
	})
	})
	function delete_staff($id){
		start_load()
		$.ajax({
			url:'ajax.php?action=delete_staff',
			method:'POST',
			data:{id:$id},
			success:function(resp){
				if(resp==1){
					alert_toast("Data successfully deleted",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
			}
		})
	}
</script>