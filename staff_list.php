<?php include'db_connect.php' ?>
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<table class="table tabe-hover table-bordered" id="list">
				<thead>
					<tr>
						<th class="text-center">#</th>
						
						<th class="text-center">Name</th>
						<th class="text-center">Contact #</th>
						<th class="text-center">Address</th>
						<th class="text-center">Email</th>
						<th class="text-center">Departments</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
					$qry = $conn->query("SELECT *,concat(lastname,', ',firstname,' ',middlename) as username, staff.ID as staff_ID FROM staff INNER JOIN departments ON departments.id = staff.department_id order by lastname asc");
					while($row= $qry->fetch_assoc()):
					?>
					
					
					<tr>
						<th class="text-center"><?php echo $i++ ?></th>
						<td class="text-center"><b><?php echo ucwords($row['username']) ?></b></td>
						<td class="text-center"><b><?php echo $row['contact'] ?></b></td>
						<td class="text-center"><b><?php echo $row['address'] ?></b></td>
						<td class="text-center"><b><?php echo $row['email'] ?></b></td>
						<td class="text-center"><b><?php echo $row['name'] ?></b></td>
						<td class="text-center">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
		                      Action
		                    </button>
							
							<div class="dropdown-menu" style="">
							
						
		                     
		                      <a class="dropdown-item review_list" href="./index.php?page=review_list&id=<?php echo $row['staff_ID'] ?>" data-id="<?php echo $row['staff_ID'] ?>">View</a>
							  <?php if($_SESSION['login_type'] != 3 && $_SESSION['login_type'] != 2 )
							{?> 
		                      <div class="dropdown-divider"></div>

		                      <a class="dropdown-item" href="./index.php?page=edit_staff&id=<?php echo $row['staff_ID'] ?>">Edit</a>
		                      <div class="dropdown-divider"></div>
		                      <a class="dropdown-item delete_staff" href="javascript:void(0)" data-id="<?php echo $row['staff_ID'] ?>">Delete</a>
		                    </div>
						</td>	
						<?php } ?>

					</tr>	
				<?php endwhile; ?>
				</tbody>
			</table>
			
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#list').dataTable()
	$('.delete_staff').click(function(){
	_conf("Are you sure to delete this staff?","delete_staff",[$(this).attr('data-id')])
	})
	})
	function delete_staff($id){
		start_load()
		$.ajax({
			url:'ajax.php?action=delete_staff',
			method:'POST',
			data:{id:$id},
			success:function(resp){
				if(resp==1){
					alert_toast("Data successfully deleted",'success')
					setTimeout(function(){
						location.reload()
					},1500)

				}
			}
		})
	}
</script>